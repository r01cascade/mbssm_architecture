#ifndef SCHELLING_THEORY_H_
#define SCHELLING_THEORY_H_

#include "Theory.h"
#include "SegregationAgent.h"

#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include "repast_hpc/GridComponents.h"

//TODO: inherit Theory


private:
	repast::SharedContext<SegregationAgent>* mpContext;
	repast::SharedDiscreteSpace<SegregationAgent, repast::StrictBorders, repast::SimpleAdder<SegregationAgent> > *mpSpace;

	//TODO: define a variable satisfaction status
	

	//TODO: define a variable for moving decision
	

public:
	SchellingTheory(repast::SharedContext<SegregationAgent>* context,
		repast::SharedDiscreteSpace<SegregationAgent, repast::StrictBorders, repast::SimpleAdder<SegregationAgent> >* space);
	~SchellingTheory();

	//TODO: override doSituation()
	

	//TODO: override doAction()
	

	bool getSatisfiedStatus();
	bool getMovingIntention();
};

#endif