#ifndef INCLUDE_BOARD_H_
#define INCLUDE_BOARD_H_

#include "StructuralEntity.h"
#include "SegregationAgent.h"

#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"

//TODO: inherit StructuralEntity


private:
	repast::SharedContext<SegregationAgent>* mpContext;
	repast::SharedDiscreteSpace<SegregationAgent, repast::StrictBorders, repast::SimpleAdder<SegregationAgent>> *mpDiscreteSpace;

	//TODO: define 2 variables for satisfaction and segregation index
	

	//TODO: define 2 functions to update avg satisfaction and segregation index
	

public:
	Board(repast::SharedContext<SegregationAgent> *context, 
		repast::SharedDiscreteSpace<SegregationAgent, repast::StrictBorders, repast::SimpleAdder<SegregationAgent>> *discreteSpace);
	
	//TODO: override doTransformation();
	

	double getAvgSatisfaction();
	double getSegregationIndex();
	
	void printBoardToScreen();
};

#endif /* INCLUDE_BOARD_H_ */