#include <stdio.h>
#include <vector>
#include <boost/mpi.hpp>
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/initialize_random.h"
#include "repast_hpc/SVDataSetBuilder.h"
#include "repast_hpc/Point.h"
#include "repast_hpc/Random.h"

#include "SegregationModel.h"
#include "SchellingTheory.h"
#include "SegregationTheoriesMediator.h"

SegregationModel::SegregationModel(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm): mContext(comm){
	mProps = new repast::Properties(propsFile, argc, argv, comm);
	mStopAt = repast::strToInt(mProps->getProperty("stop.at"));
	mCountOfAgents = repast::strToInt(mProps->getProperty("count.of.agents"));
	mBoardSize = repast::strToInt(mProps->getProperty("board.size"));
	initializeRandom(*mProps, comm);
	if(repast::RepastProcess::instance()->rank() == 0) mProps->writeToSVFile("./output/record.csv");
	
	// Create discrete space
	repast::Point<double> origin(1,1);
	repast::Point<double> extent(mBoardSize,mBoardSize);
	
	repast::GridDimensions gd(origin, extent);
	
	std::vector<int> processDims;
	processDims.push_back(1);
	processDims.push_back(1);
	
	discreteSpace = new repast::SharedDiscreteSpace<SegregationAgent, repast::StrictBorders, repast::SimpleAdder<SegregationAgent> >("AgentDiscreteSpace", gd, processDims, 0, comm);
	std::cout << "RANK " << repast::RepastProcess::instance()->rank() << " BOUNDS: " << discreteSpace->bounds().origin() << " " << discreteSpace->bounds().extents() << std::endl;
	mContext.addProjection(discreteSpace);
	
	//TODO: init at the marcro level: the Board structural entity
	
}

SegregationModel::~SegregationModel(){
	delete mProps;
}

void SegregationModel::initAgents(){
	int rank = repast::RepastProcess::instance()->rank();
	repast::IntUniformGenerator gen = repast::Random::instance()->createUniIntGenerator(1, mBoardSize);
	int countType0 = mCountOfAgents/2;
	int countType1 = mCountOfAgents - countType0;
	double threshold = repast::strToDouble(mProps->getProperty("threshold"));
	for(int i = 0; i < mCountOfAgents; i++){
		//agent starts at a random location
		int xRand, yRand;
		std::vector<SegregationAgent*> agentList;
		do {
			agentList.clear();
			xRand = gen.next();
			yRand = gen.next();
			discreteSpace->getObjectsAt(repast::Point<int>(xRand, yRand), agentList);
		} while (agentList.size() != 0);

		//assign the first N agents to type 0 then the rest to type 1
		repast::Point<int> initialLocation(xRand, yRand);
		repast::AgentId id(i, rank, 0);
		id.currentRank(rank);
		int type;
		if (countType0 > 0) {
			type = 0;
			countType0--;
		} else {
			type = 1;
			countType1--;
		}

		//TODO: init at the micro level: agent, theory, theory mediator
		
	}
}

void SegregationModel::doSituationalMechanisms(){
	std::vector<SegregationAgent*> agents;
	mContext.selectAgents(mCountOfAgents, agents);
	std::vector<SegregationAgent*>::iterator iter = agents.begin();
	while(iter != agents.end()){
		//TODO: call doSituation for each agent
		
		iter++;
	}
}

void SegregationModel::doActionMechanisms(){
	std::vector<SegregationAgent*> agents;
	mContext.selectAgents(mCountOfAgents, agents);
	std::vector<SegregationAgent*>::iterator iter = agents.begin();
	while(iter != agents.end()){
		//TODO: call doAction for each agent
		
		iter++;
	}
}

void SegregationModel::doTransformationalMechanisms() {
	//TODO: call doTransformation of the Board structural entity
	
}

void SegregationModel::doPerTick(){
	//TODO: call three mechanisms in the correct order
	

	//print to screen: satisfaction (every tick) & board (at start and end)
	double currentTick = repast::RepastProcess::instance()->getScheduleRunner().currentTick();
	if(repast::RepastProcess::instance()->rank() == 0) {
		printf("Tick: %.1f\tSatisfaction: %.3f\tSegregation index: %.3f\n", 
			currentTick, mpBoard->getAvgSatisfaction(), mpBoard->getSegregationIndex());
		
		//print board at the beginning (tick=2), at the end (tick=mStopAt), when all agents are satisfied (100% satisfaction)
		if (currentTick==2 || currentTick==mStopAt || mpBoard->getAvgSatisfaction()==1)
			mpBoard->printBoardToScreen();
		
		//stop when all agents are satisfied
		if (mpBoard->getAvgSatisfaction()==1)
			repast::RepastProcess::instance()->getScheduleRunner().stop();
	}
}

void SegregationModel::initSchedule(repast::ScheduleRunner& runner){
	//TODO: schedule actions every tick
	

	//TODO: schedule stopping condition at max tick
	
}
