#include "SegregationTheoriesMediator.h"
#include "SegregationAgent.h"

SegregationTheoriesMediator::SegregationTheoriesMediator(std::vector<Theory*> theoryList) : TheoryMediator(theoryList) {}

void SegregationTheoriesMediator::mediateSituation() {
	//TODO: trigger situation mechanisms
	

	//TODO: get the satisfaction value from the Theory object
	

	//TODO: because there is only one theory, pass satisfaction value to the agent
	
}

void SegregationTheoriesMediator::mediateAction() {
	//TODO: trigger action mechanisms
	

	//TODO: if the agent intends to move, perform the move action
	
}