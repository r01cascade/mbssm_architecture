/* Agent.h */

#ifndef MICRO_AGENT
#define MICRO_AGENT

#include "TheoryMediator.h"
#include "Theory.h"

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"

/* Agents */
class MicroAgent {
	
protected:
	repast::AgentId mId;
	TheoryMediator *mpMediator;

public:
	MicroAgent(repast::AgentId id);  // Constructor
	~MicroAgent(); // Destructor
	
	/* Required Getters */
	virtual repast::AgentId& getId(){                   return mId;    }
	virtual const repast::AgentId& getId() const{       return mId;    }

	/* Setter */
    void set(int currentRank);	
    void setMediator(TheoryMediator *mediator);

	/* Situational mechanisms */
	void doSituation();

	/* Action mechanisms */
	void doAction();
};

/* Serializable Agent Package */
struct AgentPackage{
	
public:
	int     id;
	int     rank;
	int     type;
	int     currentRank;

	/* Constructors */
	AgentPackage(); // For serialization
	AgentPackage(int _id, int _rank, int _type, int _currentRank);
	
	/* For archive packaging */
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version){
        ar & id;
        ar & rank;
        ar & type;
        ar & currentRank;
	}
};


#endif
