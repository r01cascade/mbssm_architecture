#ifndef INCLUDE_THEORY_H_
#define INCLUDE_THEORY_H_

class MicroAgent; //forward declare, but DO NOT define

class Theory {

protected:
	MicroAgent *mpAgent;

public:
	virtual ~Theory() {};

	void setAgent(MicroAgent *agent);
	virtual void doSituation() = 0;
	virtual void doAction() = 0;
};

#endif /* INCLUDE_THEORY_H_ */
