#ifndef INCLUDE_STRUCTURALENTITY_H_
#define INCLUDE_STRUCTURALENTITY_H_

#include "Regulator.h"
#include "vector"

class StructuralEntity {

public:
	StructuralEntity() {};	

	StructuralEntity(std::vector<Regulator*> regulatorList, int transformationalInterval) {
		mpRegulatorList = regulatorList;
		mTransformationalInterval = transformationalInterval;
	};

	virtual ~StructuralEntity() {
		for(std::vector<Regulator*>::iterator iter = mpRegulatorList.begin(); iter != mpRegulatorList.end(); iter++) {delete (*iter);}
	};
	virtual void doTransformation() = 0;


protected:
	std::vector<Regulator*> mpRegulatorList;
	int mTransformationalInterval;
};

#endif /* INCLUDE_STRUCTURALENTITY_H_ */
