/* Model.h */

#ifndef INCLUDE_MODEL_H_
#define INCLUDE_MODEL_H_

#include "repast_hpc/Schedule.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/AgentRequest.h"

#include "MicroAgent.h"
#include "StructuralEntity.h"

/* Agent Package Provider */
class AgentPackageProvider {
	
private:
    repast::SharedContext<MicroAgent>* agents;
	
public:
	
    AgentPackageProvider(repast::SharedContext<MicroAgent>* agentPtr);
	
    void providePackage(MicroAgent * agent, std::vector<AgentPackage>& out);
	
    void provideContent(repast::AgentRequest req, std::vector<AgentPackage>& out);
	
};

/* Agent Package Receiver */
class AgentPackageReceiver {
	
private:
    repast::SharedContext<MicroAgent>* agents;
	
public:
	
    AgentPackageReceiver(repast::SharedContext<MicroAgent>* agentPtr);
	
    MicroAgent * createAgent(AgentPackage package);
	
    void updateAgent(AgentPackage package);
	
};


class Model {
private:
	
	
protected:
	int mStopAt;
	int mCountOfAgents;
	repast::Properties* mProps;
	repast::SharedContext<MicroAgent> context;
	AgentPackageProvider* provider;
	AgentPackageReceiver* receiver;

	std::vector<StructuralEntity*> structuralEntityList; //a list of structural entity that perform transformational mechanisms
	
public:
	Model(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm);
	virtual ~Model();
	virtual void initAgents() = 0;
	void initSchedule(repast::ScheduleRunner& runner); //schedule 3 mechanisms and microsim time-related activities

	void doSituationalMechanisms();
	void doActionMechanisms();
	void doTransformationalMechanisms();
	void doPerTick();
};

#endif
