#ifndef INCLUDE_THEORYMEDIATOR_H_
#define INCLUDE_THEORYMEDIATOR_H_

class MicroAgent;
#include "Theory.h"
#include <vector>

class TheoryMediator {

protected:
	std::vector<Theory*> mTheoryList;
	MicroAgent *mpAgent;

public:
	TheoryMediator(std::vector<Theory*> theoryList);
	~TheoryMediator();
	void setAgent(MicroAgent *agent); //link agent to this mediator and all theories in the theory list

	virtual void mediateSituation() = 0;
	virtual void mediateAction() = 0;
};

#endif /* INCLUDE_THEORYMEDIATOR_H_ */
