#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"

#include "Model.h"

AgentPackageProvider::AgentPackageProvider(repast::SharedContext<MicroAgent>* agentPtr): agents(agentPtr){ }

void AgentPackageProvider::providePackage(MicroAgent * agent, std::vector<AgentPackage>& out){
	repast::AgentId id = agent->getId();
	AgentPackage package(id.id(), id.startingRank(), id.agentType(), id.currentRank());
	out.push_back(package);
}

void AgentPackageProvider::provideContent(repast::AgentRequest req, std::vector<AgentPackage>& out){
	std::vector<repast::AgentId> ids = req.requestedAgents();
	for(size_t i = 0; i < ids.size(); i++){
		providePackage(agents->getAgent(ids[i]), out);
	}
}

AgentPackageReceiver::AgentPackageReceiver(repast::SharedContext<MicroAgent>* agentPtr): agents(agentPtr){}

MicroAgent * AgentPackageReceiver::createAgent(AgentPackage package){
	repast::AgentId id(package.id, package.rank, package.type, package.currentRank);
	return new MicroAgent(id); // When agent moves to other process he/she only takes ID; all other properties will be reset
}

void AgentPackageReceiver::updateAgent(AgentPackage package){
	repast::AgentId id(package.id, package.rank, package.type);
	MicroAgent * agent = agents->getAgent(id);
	agent->set(package.currentRank);
}


Model::Model(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm): context(comm){
	mProps = new repast::Properties(propsFile, argc, argv, comm);
	mStopAt = repast::strToInt(mProps->getProperty("stop.at"));
	mCountOfAgents = repast::strToInt(mProps->getProperty("count.of.agents"));

	provider = new AgentPackageProvider(&context);
	receiver = new AgentPackageReceiver(&context);
}

Model::~Model(){
	delete mProps;
	delete provider;
	delete receiver;
}

Model::initAgents() {
	for (int i=1; i<=mCountOfAgents; i++) {
		int rank = repast::RepastProcess::instance()->rank();
		repast::AgentId id(i, rank, 0);
		id.currentRank(rank);
		Agent *agent = new Agent(id);
		context.addAgent(agent);
	}
}

void Model::doSituationalMechanisms(){
	repast::SharedContext<MicroAgent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<MicroAgent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {
		(*iter)->doSituation();
		iter++;
	}
	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}


void Model::doActionMechanisms(){
	repast::SharedContext<MicroAgent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<MicroAgent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {
		(*iter)->doAction();
		iter++;
	}
	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}


void Model::doTransformationalMechanisms() {
	std::vector<StructuralEntity*>::iterator it = structuralEntityList.begin();
	while(it != structuralEntityList.end()){
		(*it)->doTransformation();
		it++;
	}
	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}

void Model::doPerTick() {
	doSituationalMechanisms();
	doActionMechanisms();
	doTransformationalMechanisms();
}

void Model::initSchedule(repast::ScheduleRunner& runner) {
	runner.scheduleEvent(1, 1, \
			repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::doPerTick)));
	runner.scheduleStop(mStopAt);
}
