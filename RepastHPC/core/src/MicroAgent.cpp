#include "MicroAgent.h"

/* Serializable Agent Package Data */
AgentPackage::AgentPackage(){ }

AgentPackage::AgentPackage(int _id, int _rank, int _type, int _currentRank) : id(_id), rank(_rank), type(_type), currentRank(_currentRank) { }

MicroAgent::MicroAgent(repast::AgentId id) { 
	mId = id;
}

MicroAgent::~MicroAgent(){
	delete mpMediator;
}

void MicroAgent::doSituation() {
	mpMediator->mediateSituation();
}

void MicroAgent::doAction() {
	mpMediator->mediateAction();
}

void MicroAgent::setMediator(TheoryMediator *mediator) {
	mpMediator = mediator;
}