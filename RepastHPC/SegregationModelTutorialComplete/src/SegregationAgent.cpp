#include "SegregationAgent.h"

SegregationAgent::SegregationAgent(repast::AgentId id, int type, double threshold,repast::SharedDiscreteSpace<SegregationAgent, repast::StrictBorders, repast::SimpleAdder<SegregationAgent> >* space) : MicroAgent(id), mAgentType(type), mThreshold(threshold), mSpace(space) {}

SegregationAgent::~SegregationAgent() {}

void SegregationAgent::setSatisfiedStatus(bool satisfiedStatus) {mIsSatisfied = satisfiedStatus;}
int SegregationAgent::getType() {return mAgentType;}
double SegregationAgent::getThreshold() {return mThreshold;}
bool SegregationAgent::getSatisfiedStatus() {return mIsSatisfied;}

void SegregationAgent::move() {
	//get this agent location
	std::vector<int> agentLoc;
	mSpace->getLocation(mId, agentLoc);
	
	//find a random empty position
	int xMax = (int)mSpace->bounds().extents(0) - 1;
	repast::IntUniformGenerator gen = repast::Random::instance()->createUniIntGenerator(1, xMax);
	std::vector<int> agentNewLoc;
	do {
		agentNewLoc.clear();
		
		int xRand, yRand;
		std::vector<SegregationAgent*> agentList;
		do {
			agentList.clear();
			xRand = gen.next();
			yRand = gen.next();
			mSpace->getObjectsAt(repast::Point<int>(xRand, yRand), agentList);
		} while (agentList.size() != 0);

		agentNewLoc.push_back(xRand);
		agentNewLoc.push_back(yRand);

		// Note: checking to see if agent would move outside GLOBAL bounds; exceeding local bounds is OK
		if(!mSpace->bounds().contains(agentNewLoc)) std::cout << " INVALID: " << agentNewLoc[0] << "," << agentNewLoc[1] << std::endl;
		
	} while(!mSpace->bounds().contains(agentNewLoc));
	
	//TODO: move to a random empty position
	mSpace->moveTo(mId,agentNewLoc);
}
