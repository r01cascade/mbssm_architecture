#include "SchellingTheory.h"

#include "repast_hpc/Moore2DGridQuery.h"
#include "repast_hpc/Point.h"

SchellingTheory::SchellingTheory(repast::SharedContext<SegregationAgent>* context,
		repast::SharedDiscreteSpace<SegregationAgent, repast::StrictBorders, repast::SimpleAdder<SegregationAgent> >* space) {
	mpContext = context;
	mpSpace = space;
}

SchellingTheory::~SchellingTheory(){}

void SchellingTheory::doSituation() {
	//get this agent location
	std::vector<int> agentLoc;
	mpSpace->getLocation(mpAgent->getId(), agentLoc);

	//get neigbours
	std::vector<SegregationAgent*> neighbourAgents;
	repast::Point<int> center(agentLoc);
	repast::Moore2DGridQuery<SegregationAgent> moore2DQuery(mpSpace);
	moore2DQuery.query(agentLoc, 1, false, neighbourAgents);
	
	//count similar
	int similarCount = 0;
	std::vector<SegregationAgent*>::iterator agentIter = neighbourAgents.begin();
	while(agentIter != neighbourAgents.end()) {
		std::vector<int> otherLoc;
		mpSpace->getLocation((*agentIter)->getId(), otherLoc);
		repast::Point<int> otherPoint(otherLoc);
		
		//TODO: increase the count if 2 agents are the same type
		if ((*agentIter)->getType() == ((SegregationAgent*)mpAgent)->getType()) {
			similarCount++;
		}

		agentIter++;
	}

	//TODO: if similarity >= threshold, update satisfaction
	double similarityPercentage = (double)similarCount/neighbourAgents.size();
	double threshold = ((SegregationAgent*)mpAgent)->getThreshold();
	mIsSatisfied = (similarityPercentage >= threshold);
}

void SchellingTheory::doAction() {
	//TODO: if not satisfied, moving intention =TRUE else =FALSE
	mMovingIntention = ! mIsSatisfied;
}

bool SchellingTheory::getSatisfiedStatus() {
	return mIsSatisfied;
}

bool SchellingTheory::getMovingIntention() {
	return mMovingIntention;
}