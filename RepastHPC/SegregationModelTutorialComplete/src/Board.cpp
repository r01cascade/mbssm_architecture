#include "Board.h"
#include "SegregationAgent.h"

#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"

#include <cmath>

Board::Board(repast::SharedContext<SegregationAgent> *context,
		repast::SharedDiscreteSpace<SegregationAgent, repast::StrictBorders, repast::SimpleAdder<SegregationAgent>> *discreteSpace) : StructuralEntity() {
	mpContext = context;
	mpDiscreteSpace = discreteSpace;
}

void Board::doTransformation() {
	//TODO: transformational mechanisms: update avg satisfaction and segregation index
	updateAvgSatisfaction();
	updateSegregationIndex();
}

double Board::getAvgSatisfaction() {
	return mAvgSatisfaction;
}

double Board::getSegregationIndex() {
	return mSegregationIndex;
}

void Board::updateAvgSatisfaction() {
	repast::SharedContext<SegregationAgent>::const_local_iterator iter = mpContext->localBegin();
	repast::SharedContext<SegregationAgent>::const_local_iterator iterEnd = mpContext->localEnd();
	
	//calculate average satisfaction
	mAvgSatisfaction = 0;
	while (iter != iterEnd) {
		mAvgSatisfaction += (*iter)->getSatisfiedStatus();
		iter++;
	}
	mAvgSatisfaction /= mpContext->size();
}

void Board::updateSegregationIndex() {
	int x0 = mpDiscreteSpace->bounds().origin().getX();
	int y0 = mpDiscreteSpace->bounds().origin().getY();
	int x1 = 0;
	int y1 = 0;
	int maxX = mpDiscreteSpace->bounds().extents().getX();
	int maxY = mpDiscreteSpace->bounds().extents().getY();
	int windowSize = 5;

	//count type0 & type1
	int countType0 = 0;
	int countType1 = 0;
	std::vector<SegregationAgent*> agentList;
	for (int x=x0; x<=maxX; x++) {
		for (int y=y0; y<=maxY; y++) {
			agentList.clear();
			mpDiscreteSpace->getObjectsAt(repast::Point<int>(x, y), agentList);
			if (agentList.size() > 1) {std::cerr << "More than 1 agent per cell" << std::endl;}
			if (agentList.size() > 0) {
				if ((agentList.front())->getType() == 0)
					countType0++;
				else if ((agentList.front())->getType() == 1)
					countType1++;
			}
		}
	}

	//sweep areas (defined by window size)
	double segregationIndex = 0;
	while (true) {
		//determine window coordinates: (x0,y0) to (x1,y1)
		x1 = x0 + windowSize - 1;
		y1 = y0 + windowSize - 1;

		//cap if out of range
		if (x1 > maxX) x1 = maxX;
		if (y1 > maxY) y1 = maxY;

		//printf("Zone coords: (%d,%d) to (%d,%d)\n", x0,y0,x1,y1);

		//find local count by type
		int localCountType0 = 0;
		int localCountType1 = 0;
		for (int x=x0; x<=x1; x++)
			for (int y=y0; y<=y1; y++) {
				agentList.clear();
				mpDiscreteSpace->getObjectsAt(repast::Point<int>(x, y), agentList);
				if (agentList.size() > 1) {std::cerr << "More than 1 agent per cell" << std::endl;}
				if (agentList.size() > 0) {
					if ((agentList.front())->getType() == 0)
						localCountType0++;
					else if ((agentList.front())->getType() == 1)
						localCountType1++;
				}
		}

		//add to segregation index
		//printf("Partial segregation: %.5f\n", abs((double)localCountType0/countType0 - (double)localCountType1/countType1));
		segregationIndex += abs((double)localCountType0/countType0 - (double)localCountType1/countType1);

		//exit if the window reaches the max coords
		if (x1 == maxX && y1 == maxY)
			break;

		//next window coordinates
		x0 = x0 + windowSize; // move along x-axis
		if (x0 > maxX) { // when x is out of range
			x0 = mpDiscreteSpace->bounds().origin().getX(); // reset x
			y0 = y0 + windowSize; //move along y-axis
		}
	}

	//final value
	mSegregationIndex = segregationIndex / 2;
}

void Board::printBoardToScreen() {
	int mBoardSize = mpDiscreteSpace->bounds().extents().getX() - mpDiscreteSpace->bounds().origin().getX();
	std::vector<SegregationAgent*> agentList;
	for (int i=0; i<=mBoardSize+1; i++) {
		for (int j=0; j<=mBoardSize+1; j++) {
			if (i==0 || i==mBoardSize+1)
				std::cout << "-";
			else if (j==0 || j==mBoardSize+1)
				std::cout << "|";
			else {
				agentList.clear();
				mpDiscreteSpace->getObjectsAt(repast::Point<int>(i, j), agentList);
				if (agentList.size() > 1) {std::cerr << "More than 1 agent per cell" << std::endl;}
				if (agentList.size() == 0)
					std::cout << " ";
				else if ((agentList.front())->getType() == 0)
					std::cout << "X";
				else if ((agentList.front())->getType() == 1)
					std::cout << ".";
			}
		}
		std::cout << std::endl;
	}
}