#include "SegregationTheoriesMediator.h"
#include "SegregationAgent.h"

SegregationTheoriesMediator::SegregationTheoriesMediator(std::vector<Theory*> theoryList) : TheoryMediator(theoryList) {}

void SegregationTheoriesMediator::mediateSituation() {
	//TODO: trigger situation mechanisms
	((SchellingTheory*) mTheoryList[0])->doSituation();

	//TODO: get the satisfaction value from the Theory object
	bool updatedSatisfation = ((SchellingTheory*) mTheoryList[0])->getSatisfiedStatus();

	//TODO: because there is only one theory, pass satisfaction value to the agent
	((SegregationAgent*) mpAgent)->setSatisfiedStatus(updatedSatisfation);
}

void SegregationTheoriesMediator::mediateAction() {
	//TODO: trigger action mechanisms
	mTheoryList[0]->doAction();

	//TODO: if the agent intends to move, perform the move action
	if (((SchellingTheory*) mTheoryList[0])->getMovingIntention())
		((SegregationAgent*) mpAgent)->move();
}