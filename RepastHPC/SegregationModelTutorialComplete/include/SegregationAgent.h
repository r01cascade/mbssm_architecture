#ifndef SCHELLING_AGENT
#define SCHELLING_AGENT

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"

#include "MicroAgent.h"

//TODO: inherit MicroAgent
class SegregationAgent : public MicroAgent {

private:
	//TODO: define 3 variables: agent type, satisfied status, threshold
	int mAgentType;
	bool mIsSatisfied;
	double mThreshold;

	repast::SharedDiscreteSpace<SegregationAgent, repast::StrictBorders, repast::SimpleAdder<SegregationAgent> >* mSpace;

public:
	SegregationAgent(repast::AgentId id, int mAgentType, double threshold,
			repast::SharedDiscreteSpace<SegregationAgent, repast::StrictBorders, repast::SimpleAdder<SegregationAgent> >* space);
	~SegregationAgent();
	
	/* Getters specific to this kind of Agent */
	int getType();
	double getThreshold();
	bool getSatisfiedStatus();

	/* Setter */
	void setSatisfiedStatus(bool satisfiedStatus);
	
	/* Actions */
	//TODO: define move() function
	void move();
};

/* Serializable Agent Package */
struct SegregationAgentPackage {
	
public:
	int	id;
	int	rank;
	int	type;
	int	currentRank;
	int agentType;
	bool isSatisfied ;
	
	/* Constructors */
	SegregationAgentPackage(); // For serialization
	SegregationAgentPackage(int _id, int _rank, int _type, int _currentRank, int _agentType, bool _isSatisfied);
	
	/* For archive packaging */
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version){
		ar & id;
		ar & rank;
		ar & type;
		ar & currentRank;
		ar & agentType;
		ar & isSatisfied;
	}
	
};

#endif