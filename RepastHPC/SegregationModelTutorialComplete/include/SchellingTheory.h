#ifndef SCHELLING_THEORY_H_
#define SCHELLING_THEORY_H_

#include "Theory.h"
#include "SegregationAgent.h"

#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include "repast_hpc/GridComponents.h"

//TODO: inherit Theory
class SchellingTheory : public Theory {

private:
	repast::SharedContext<SegregationAgent>* mpContext;
	repast::SharedDiscreteSpace<SegregationAgent, repast::StrictBorders, repast::SimpleAdder<SegregationAgent> > *mpSpace;

	//TODO: define a variable satisfaction status
	bool mIsSatisfied;

	//TODO: define a variable for moving decision
	bool mMovingIntention;

public:
	SchellingTheory(repast::SharedContext<SegregationAgent>* context,
		repast::SharedDiscreteSpace<SegregationAgent, repast::StrictBorders, repast::SimpleAdder<SegregationAgent> >* space);
	~SchellingTheory();

	//TODO: override doSituation()
	void doSituation() override;

	//TODO: override doAction()
	void doAction() override;

	bool getSatisfiedStatus();
	bool getMovingIntention();
};

#endif