#ifndef SCHELLING_MODEL
#define SCHELLING_MODEL

#include <boost/mpi.hpp>
#include "repast_hpc/Schedule.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/AgentRequest.h"
#include "repast_hpc/TDataSource.h"
#include "repast_hpc/SVDataSet.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include "repast_hpc/GridComponents.h"

#include "SegregationAgent.h"
#include "Board.h"

class SegregationModel {
private:
	int mStopAt;
	int mCountOfAgents;
	int mBoardSize;
	repast::Properties* mProps;

	//TODO: define the context for agents
	repast::SharedContext<SegregationAgent> mContext;

	//TODO: define the Board structural entity
	Board* mpBoard;

	repast::SharedDiscreteSpace<SegregationAgent, repast::StrictBorders, repast::SimpleAdder<SegregationAgent> >* discreteSpace;
	
public:
	SegregationModel(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm);
	~SegregationModel();

	void doSituationalMechanisms();
	void doActionMechanisms();
	void doTransformationalMechanisms();
	
	//TODO: define a function to perform actions every tick
	void doPerTick();

	//TODO: define a function to init agents
	void initAgents();

	//TODO: define a function to init schedulers
	void initSchedule(repast::ScheduleRunner& runner);
};

#endif
