#ifndef MEDIATOR_FOR_ONE_THEORY_H_
#define MEDIATOR_FOR_ONE_THEORY_H_

#include "TheoryMediator.h"
#include "Theory.h"
#include "SchellingTheory.h"

//TODO: inherit TheoryMediator
class SegregationTheoriesMediator : public TheoryMediator {

public:
	//TODO: define the constructor
	SegregationTheoriesMediator(std::vector<Theory*> theoryList);
	
	//TODO: override mediateSituation()
	void mediateSituation() override;

	//TODO: override mediateAction()
	void mediateAction() override;
};

#endif