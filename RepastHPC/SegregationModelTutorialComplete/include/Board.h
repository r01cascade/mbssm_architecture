#ifndef INCLUDE_BOARD_H_
#define INCLUDE_BOARD_H_

#include "StructuralEntity.h"
#include "SegregationAgent.h"

#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"

//TODO: inherit StructuralEntity
class Board : public StructuralEntity {

private:
	repast::SharedContext<SegregationAgent>* mpContext;
	repast::SharedDiscreteSpace<SegregationAgent, repast::StrictBorders, repast::SimpleAdder<SegregationAgent>> *mpDiscreteSpace;

	//TODO: define 2 variables for satisfaction and segregation index
	double mAvgSatisfaction;
	double mSegregationIndex;

	//TODO: define 2 functions to update avg satisfaction and segregation index
	void updateAvgSatisfaction();
	void updateSegregationIndex();

public:
	Board(repast::SharedContext<SegregationAgent> *context, 
		repast::SharedDiscreteSpace<SegregationAgent, repast::StrictBorders, repast::SimpleAdder<SegregationAgent>> *discreteSpace);
	
	//TODO: override doTransformation();
	void doTransformation() override;

	double getAvgSatisfaction();
	double getSegregationIndex();
	
	void printBoardToScreen();
};

#endif /* INCLUDE_BOARD_H_ */