# About
This is the source code for the MBSSM architecture (Mechanism-Based Social System Modelling) by [CASCADE project](https://www.sheffield.ac.uk/cascade) for the following publication in the Journal of Artificial Societies and Social Simulation (JASSS).

Vu, Tuong Manh, Probst, Charlotte, Nielsen, Alexandra, Bai, Hao, Meier, Petra S., Buckley, Charlotte, Strong, Mark, Brennan, Alan and Purshouse, Robin C. (2020) 'A Software Architecture for Mechanism-Based Social Systems Modelling in Agent-Based Simulation Models' Journal of Artificial Societies and Social Simulation 23 (3) 1 <http://jasss.soc.surrey.ac.uk/23/3/1.html>. doi: 10.18564/jasss.4282

This repo is hereby licensed for use under the GNU GPL version 3. We are open to contribution via pull requests. Implementations of MBSSM in other simulation toolkits and other programming languagues are welcomed. You can also contribute to the example model.

# MBSSM implementation in RepastHPC
* The "core" classes in the papers are implemented in RepastHPC. These are available in ./RepastHPC/core/

# Schelling segregation model tutorial in RepastHPC
* A hands-on tutorial for MBSSM model is available at [r01cascade.bitbucket.io](https://r01cascade.bitbucket.io/)
* The source code for the turoial is in the folder RepastHPC of this repo. The folder to start the tutorial is ./RepastHPC/SchellingModelTutorial/. The folder for a complete version (after finishing the tutorial) is ./RepastHPC/SchellingModelTutorialComplete/

# Design of the Schelling segregation model
* The phenomenon of residential segregation appears as emerging pattern in the real world considering different segregation criteria (educational level, social religion, annual income, economic status, ethnicity, political point of view, etc). In 1971, Thomas Schelling created an agent-based model that might help explain why segregation is so difficult to combat. An agent represent a household. Each agent lives on a square of a 2D grid. There are two types of agents on the board. The agent follows a simple rule: if a predefined number of my neighbours are not like me, I am unsatisfied and move to a random place on the board.
* Our example uses RepastHPC's DiscreteSpace to set up a 2D board where each cell can only contain one agent.
* There are two types of agents (1:1 ratio). If neighbours of an agent is less than a pre-defined threshold, agents will move to a random location on the board.
* We only implemented one "theory" about agent satisfaction: counting similar agents in the 8 neighbours and compare to a threshold. Thus, we do not have to mediate results from multiple theories. Even the MBSSM architecture is designed for multi-theories, it can be used in this case by designing a simple mediator for one theory. This is also to demonstrate how the mediator works.
* The three mechanisms are _(1) situational mechanism_ — agents observe the proportion of nearby agents with the same characteristic; _(2) action mechanism_ — the agents compare the proportion against their own preference for similar neighbours and move to a random location if preferences are not met; _(3) transformational mechanism_ — in the aggregate, household movement alters the spatial distribution of households in the city and this rearrangement of spatial clustering can be quantified using the segregation index. As the chain of mechanisms are repeated for all households, the board changes in character and the segregation pattern can emerge over time. This can be captured by a segregation index.
* You can find the implementation of situational and actional mechanisms in MediatorForOneTheory::mediateSituation() and MediatorForOneTheory::mediateAction(). The transformational mechanisms are implemeted in  Board::doTransformation().
* As for the classes, we did inherit these classes in core: MicroAgent, Theory, TheoryMediator, StructuralEntity. We did not resue the Model class but create another (very similar) version in the SchellingModel class.
* While RepastHPC supports multi core, this example was designed to be run with 1 core for simplification. User can control some model parameters in the file ./props/model.props

# Set up the RepastHPC
* Please install [RepastHPC](https://repast.github.io/repast_hpc.html). We have 2 important notes that might help with you installation.
* Installation Note 1: For our system (Ubuntu 16), before install netcdf with "./install.sh netcdf", you need to install zlib1g-dev with this command "sudo apt-get install zlib1g-dev"
* Installation Note 2: Before compiling, you need to add the relevant paths to the PATH and LD_LIBRARY_PATH. You can execute the following two lines (everytime you reset the machine) or add them to "~/.bashrc" file to automatically append.
```
export PATH=$HOME/sfw/MPICH/bin:$PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/$HOME/sfw/Boost/Boost_1.61/lib/:/$HOME/sfw/repast_hpc-2.3.0/lib/
```


# Set up and run the the complete version of Schelling model
* Download or clone the repo.
* Both core and SchellingModelTutorialComplete folders are required for compiling.
* Use `cd` command to __change directory to the SchellingModelTutorialComplete__
* Please edit the "env" file in SchellingModelTutorialComplete to match with your installation paths and versions of RepastHPC and Boost.
* If this is __the first time__, please , use the following command to clean files, create relevant folders, and compile the model: (if not, you can use `make compile` for just compiling.)
```
make all
```
* Run the model with the following command: 
```
mpirun -n 1 ./bin/main.exe ./props/config.props ./props/mode.props
```
* You can check the simulated data in the "outputs" folder. The satisfaction level and the board will be also be printed on screen. The cell with no agent is empty. The two types are marked with a cross (X) and a dot (.). For examples, the default setting (30x30 board, 765 agents (85% of 900 total cells), satisfaction threshold of 30% similarity) will show the following on screen. The experiment shows that the satisfaction increases and the segregation pattern emerges over time by comparing the board at the beginning and the end of the simulation run.
* You can do experiment by changing the value in ./props/model.props file. (We suggest increaes the threshold from 0.3 to 0.6, and observe the segregation pattern) Then re-run the model with `mpirun` command (no need to re-compile). Please make sure the number of agents is less than the area of the board and choose a board size so it fits your screen.

# Other technical notes:
* When modifying the source code, use this command for faster compiling time.
```
make compile
```
* If a class is added (a new cpp file), "makefile" needs to be updated.
* The compiling use header and source files in both core and SchellingModelTutorialComplete folder, but the all the object files \*.o are in "./SchellingModelTutorialComplete/objects/" folder and the executable file main.exe is in "./SchellingModelTutorialComplete/bin/" folder.